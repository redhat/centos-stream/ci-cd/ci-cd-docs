:_module-type: PROCEDURE

[id="proc_how-to-enable-dist-git-tests-on-merge-request_{context}"]
= How to enable dist-git tests on merge request

[role="_abstract"]
Zuul CI for CentOS Stream supports running dist-git
tests defined via STI or tmt interfaces. See
https://docs.fedoraproject.org/en-US/ci/ for details. In this article
we describe how to enable running of a distgit test on Merge Request
via Zuul.

.Prerequisites

* Write STI or tmt test for the component. We recommend https://docs.fedoraproject.org/en-US/ci/tmt/[tmt interface] for new tests as it is actively developed.

.Procedure

* Verify that the test is working
+
You can enable the test pipeline temporary for a specific Merge Request. To do so add the following line in the description of the Merge Request:
+
----
Depends-On: https://gitlab.com/redhat/centos-stream/ci-cd/zuul/jobs/-/merge_requests/34
----
+
Zuul will pick up additional unmerged configuration during the test execution, which will enable test jobs.

* Opt in to enable test jobs for a component
+
Submit a change to https://gitlab.com/redhat/centos-stream/ci-cd/zuul/jobs-config/-/blob/master/zuul.d/projects.yaml[jobs-config/zuul.d/projects.yaml]:
+
[source,yaml]
----
- project:
    name: redhat/centos-stream/rpms/<your component>
    templates:
      - build
      - lint
      - test    <1>
----
<1> Here `test` template contains set of jobs which provide STI/tmt support

[role="_additional-resources"]
.Additional resources
* https://tmt.readthedocs.io/en/stable/[tmt documentation]
